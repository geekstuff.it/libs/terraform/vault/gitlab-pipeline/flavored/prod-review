terraform {
  required_version = "~> 1.2"
}

module "baked-prod-review" {
  source = "git::https://gitlab.com/geekstuff.it/libs/terraform/vault/gitlab-pipeline/low-level/multi-env?ref=v0.0.3"

  envs = [
    {
      name = "prod"
      jwt_bound_claims = {
        ref_protected = true
      }
      enable_dev_approle = false
    },
    {
      name = "review"
      jwt_bound_claims = {
        ref_type = "branch"
      }
      enable_dev_approle = false
    },
    {
      name = "dev"
      jwt_bound_claims = {
        ref_protected = true
      }
      enable_dev_approle = true
    }
  ]
  gitlab_vault_jwt_project_id = var.gitlab_vault_jwt_project_id
  project_name                = var.project_name
  read_secrets                = var.read_secrets
  transit_keys                = var.transit_keys
}
